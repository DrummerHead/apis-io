var apis = window.apisData;
var dom = '';

for(var i = 0, l = apis.length; i < l; i++){
  var item = apis[i];
  dom += `<li>
    <img src='${item.image}'>
    <dl>
      <dt>Name</dt>
      <dd><a href='${item.humanURL}'>${item.name}</a></dd>
      <dt>Api URL</dt>
      <dd><a href='${item.baseURL}'>${item.baseURL}</a></dd>
      ${
        (function(tags){
          if(Array.isArray(tags) && tags.length > 0){
            return `<dt>Tags</dt>
            <dd>
              <ul class='tags'>
                ${(function(tags){
                  return tags.map(function(curr){
                    return '<li>' + curr + '</li>';
                  }).reduce(function(prev, curr){
                    return prev + curr;
                  });
                })(tags)}
              </ul>
            </dd>`
          }
          else if(typeof tags === 'string'){
            return `<dt>Tags</dt>
            <dd>
              <ul class='tags'>
                ${tags.replace(/^/, '<li>').replace(/,/g, '<li>').replace(/$/, '</li>')}
              </ul>
            </dd>`
          }
          else{
            return ''
          }
        })(item.tags)
      }
      <dt>Description</dt>
      <dd>${item.description}</dd>
    </dl>
  </li>`
}
console.log(apis.length)

document.getElementById('list').insertAdjacentHTML('beforeEnd', dom);
